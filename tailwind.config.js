module.exports = {
  content: ["./src/**/*.{html,js}"],
  theme: {
    extend: {
      fontFamily:{
        'utama':['Quicksand']
      },
      colors:{
        'birutua':'#0E1630',
        'orange':'#EB4A4B',
        'putih':'#FFFFFF',
        'abu':'#808DAD'
      }
    },
  },
  plugins: [],
}
